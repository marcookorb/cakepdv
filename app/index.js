// @flow
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, hashHistory, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import configureStore from './store/configureStore';
import './app.global.css';
import './stubs/COURSES'
import {green400, green500, green600, green700, green800, green900} from 'material-ui/styles/colors';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

//
// const store = configureStore();
// const history = syncHistoryWithStore(hashHistory, store);

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: green400,
    primary2Color: green500,
    primary3Color: green600,
    accent1Color: green700,
    accent2Color: green800,
    accent3Color: green900
  }
});

const rootRoute = {
  childRoutes: [ {
    path: '/',
    component: require('./components/App'),
    childRoutes: [
      require('./routes/Sales'),
      require('./routes/Course'),
      require('./routes/Customers'),
      require('./routes/Products'),
      require('./routes/Reports')
    ]
  },
  {
    path: '/login',
    component: require('./components/Login')
  }
 ]
}


render(
  <MuiThemeProvider muiTheme={muiTheme}>
    <Router
      history={hashHistory}
      routes={rootRoute}
    />
  </MuiThemeProvider>,
  document.getElementById('root')
);
