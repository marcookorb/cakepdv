import React, { Component } from 'react'
import logo from '../../resources/cake.png';

class Logo extends Component {
  render() {
    return(
      <div style={{ ...this.props.styles, float: 'left' }}>
        <img style={{ width: '100px' }} src={logo}/>
      </div>
    )
  }
}

module.exports = Logo
