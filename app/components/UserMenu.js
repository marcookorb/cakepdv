import React, { Component } from 'react';

class UserMenu extends Component {
  render() {

    const { user } = this.props;

    return(
      <div style={{ display: 'inline-block', fontSize: '16px', padding: 'inherit'}}>
        <span>{user.name}</span>
      </div>
    );
  }
}

module.exports = UserMenu
