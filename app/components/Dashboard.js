import React, { Component } from 'react'
import { Link } from 'react-router'

class Dashboard extends Component {
  render() {
    const { courses } = this.props

    return (
      <div>
        <h2>Novo Pdv do Cake</h2>
        <p>
          Mais bonito, mais flexível, mais rápido.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mollis metus sapien. Sed euismod a nibh at dignissim. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam at semper augue, vitae faucibus dolor. Suspendisse ac quam et tortor malesuada faucibus. Curabitur sit amet tristique magna. Aenean metus ex, tempus sit amet congue eget, molestie ut metus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus arcu velit, viverra ac mauris at, vestibulum volutpat justo.
        </p>
        <p>
          Integer feugiat venenatis elit, mollis auctor nisl fermentum et. Vivamus sed rhoncus turpis. Vestibulum egestas placerat sapien, vitae finibus tellus commodo et. Donec at mollis tortor, vel aliquet sem. Cras ut elit venenatis, vulputate ex ut, interdum mi. Sed sodales finibus arcu ut egestas. Duis non ipsum vel felis vehicula tempus. Nam sed malesuada risus. Pellentesque purus risus, malesuada id nulla vulputate, feugiat luctus nisl. Pellentesque risus diam, malesuada in mollis aliquet, fermentum eu quam. Vivamus non maximus odio. Nulla neque ex, facilisis id nibh sit amet, dignissim mollis purus. Duis sit amet sapien scelerisque, blandit nulla at, hendrerit metus. Quisque posuere ac orci porttitor fringilla. Ut bibendum neque eget ipsum semper pulvinar. Pellentesque tincidunt dictum nulla non placerat.
        </p>
        <h2>Courses</h2>{' '}
        <ul>
          {courses.map(course => (
            <li key={course.id}>
              <Link to={`/course/${course.id}`}>{course.name}</Link>
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

export default Dashboard
