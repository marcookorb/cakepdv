import React, { Component } from 'react'
import { Link } from 'react-router'

const dark = 'hsl(200, 20%, 20%)'
const light = '#fff'
const styles = {}

styles.wrapper = {
  marginTop: '84px',
  position: 'absolute',
  width: '10%',
  height: '100%',
  padding: 0,
  overflow: 'hidden',
  background: '#226a00',
  color: light,
  boxShadow: '3px 0 3px -3px #888'

}

styles.listWrapper = {
  margin: 0,
  width: '100%',
  padding: 0
}

styles.linkWrapper = {
  listStyle: 'none',
  height: '100px',
  margin: 0,
  width: '100%',
  background: '#41cc00'
}

styles.linkHome = {
  ...styles.linkWrapper,
  background: '#41cc00'
}

styles.linkSales = {
  ...styles.linkWrapper,
  background: '#39b300'
}

styles.linkCustomers = {
  ...styles.linkWrapper,
  background: '#339f00'
}

styles.linkProducts = {
  ...styles.linkWrapper,
  background: '#2d8d00'
}

styles.linkReports = {
  ...styles.linkWrapper,
  background: '#298000'
}

styles.linkFiscal = {
  ...styles.linkWrapper,
  background: '#257500'
}

styles.link = {
  display: 'block',
  height: 'inherit',
  padding: 11,
  color: light,
  fontWeight: 500,
  textDecoration: 'none'
}

styles.activeLink = {
  ...styles.link,
  color: dark
}

class GlobalNav extends Component {

  constructor(props, context) {
    super(props, context)
    this.logOut = this.logOut.bind(this)
  }

  logOut() {
    alert('log out')
  }

  render() {
    const { user } = this.props

    return (
      <div style={styles.wrapper}>
        <ul style={styles.listWrapper}>
          <li style={styles.linkHome}>
            <Link to="/" style={styles.link}>Home</Link>{' '}
          </li>
          <li style={styles.linkSales}>
            <Link to="/sales" style={styles.link} activeStyle={styles.activeLink}>Vender</Link>{' '}
          </li>
          <li style={styles.linkCustomers}>
            <Link to="/customers" style={styles.link} activeStyle={styles.activeLink}>Clientes</Link>{' '}
          </li>
          <li style={styles.linkProducts}>
            <Link to="/products" style={styles.link} activeStyle={styles.activeLink}>Produtos</Link>{' '}
          </li>
          <li style={styles.linkReports}>
            <Link to="/reports" style={styles.link} activeStyle={styles.activeLink}>Relatórios</Link>{' '}
          </li>
        </ul>
      </div>
    )
  }
}

GlobalNav.defaultProps = {
  user: {
    id: 1,
    name: 'Ryan Florence'
  }
}

export default GlobalNav
