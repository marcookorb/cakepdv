import React, { Component } from 'react'
import { Link } from 'react-router'
import Timer from './Timer';
import Logo from './Logo';
import UserMenu from './UserMenu';

const dark = 'hsl(200, 20%, 20%)'
const light = '#fff'
const styles = {}

styles.wrapper = {
  width: '100%',
  position: 'absolute',
  height: '84px',
  overflow: 'hidden',
  boxShadow: 'rgba(0, 0, 0, 0.74902) 0px 0px 4px 0px',
  zIndex: 1
}
// <Link to="/login">Sair</Link>
class Header extends Component {
  render() {
    const { user } = this.props

    return (
      <div style={styles.wrapper}>
        <Logo styles={{padding: '1.875em', height: 'inherit'}}/>
        <div style={{ float: 'right', padding: '1.188em', height: 'inherit' }}>
          <UserMenu user={user}/>
          <Timer />

        </div>
      </div>
    )
  }
}

Header.defaultProps = {
  user: {
    id: 1,
    name: 'Marco'
  }
}

export default Header
