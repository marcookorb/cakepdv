/*globals COURSES:true */
import React, { Component } from 'react';
import { Link } from 'react-router';
import Logo from './Logo';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import injectTapEventPlugin from 'react-tap-event-plugin';
import bgLogin from '../../resources/bg-login.png';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

const bgLoginUrl = '../resources/bg-login.png';

const styles = {};

styles.loginWrapper = {

  width: '100%',
  height: 'inherit',
  display: 'flex',
  height: '100%',
  flexDirection: 'row',
  overflow: 'hidden'
};

styles.loginContainer = {
  flex: '1',
  backgroundColor: '#d6ffd6',
  backgroundImage: 'url(' + bgLogin + ')'
};

styles.loginForm = {
  width: '416px',
  maxWidth: '416px',
  minWidth: '416px',
  height: '100%',
  boxShadow: '0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)',
  background: '#FFF',
  padding: '128px 48px 48px'
}

class Login extends Component {
  render() {
    return (
      <div style={{height:'100%', overflow: 'hidden', width: '100%'}}>
        <div style={styles.loginWrapper}>
          <div style={styles.loginContainer}></div>
          <div style={styles.loginForm}>
            <Logo />
            <TextField
              floatingLabelText="E-mail"
            />
            <br/>
            <br/>
            <TextField
              floatingLabelText="Senha"
              type="password"
            />
            <br/>
            <br/>
            <Link to="/"><RaisedButton label="Entrar" primary={true}/></Link>
          </div>
        </div>
      </div>
    )
  }
}

module.exports = Login
