/*globals COURSES:true */
import React, { Component } from 'react'
import Dashboard from './Dashboard'
import GlobalNav from './GlobalNav'
import Header from './Header'

class App extends Component {
  render() {
    return (
      <div style={{height:'100%', overflow: 'hidden', width: '100%'}}>
        <Header />
        <GlobalNav />
        <div style={{ marginTop: '84px', marginLeft: '10%', height: 'inherit' }}>
          {this.props.children || <Dashboard courses={COURSES} />}
        </div>
      </div>
    )
  }
}

module.exports = App
