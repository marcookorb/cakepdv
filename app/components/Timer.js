
import React from 'react';

const timerStyle = {
  display: 'inline-block',
  color: 'black',
  fontSize: '24px',
  fontWeight: 'lighter',
  paddingLeft:'6px'
};

class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {now: new Date().toLocaleTimeString()};
  }
  tick() {
    this.setState({
      now: new Date().toLocaleTimeString()
    });
  }
  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
  render() {
    return (
      <div style={timerStyle}>{this.state.now}</div>
    );
  }
}

export default Timer;
