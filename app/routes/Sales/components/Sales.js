import React, { Component } from 'react'
import LeftContainer from '../routes/LeftContainer/components/LeftContainer';
import RightContainer from '../routes/RightContainer/components/RightContainer';

class Sales extends Component {
  constructor(props) {
    super(props);

    this.addItem = this.addItem.bind(this);

    this.state = {
      items: []
    }
  }
  addItem(item) {
    this.setState({
      items: [...this.state.items, item]
    })
  }
  render() {

    return (
      <div style={{display: 'flex', height: 'inherit'}}>
        <LeftContainer
          addItem={this.addItem}
          items={this.state.items}
        />
        <RightContainer items={this.state.items}/>
      </div>
    )
  }
}

module.exports = Sales
