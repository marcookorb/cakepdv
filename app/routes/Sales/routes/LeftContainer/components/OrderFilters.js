import React, { Component } from 'react';
import AutoComplete from 'material-ui/AutoComplete';

const styles = {};

styles.container = {
  backgroundColor: '#ffffff'
};

class OrderFilters extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: ''
    }

    this.handleSelect = this.handleSelect.bind(this)
    this.handleUpdateInput = this.handleUpdateInput.bind(this)
  }
  handleSelect(item) {

    this.setState({
      searchText: ''
    })

    this.props.addItem({
      id: item.value,
      name: item.text
    })
  }
  handleUpdateInput(text) {
    this.setState({
      searchText: text
    })
  }
  render() {
    return(
      <div style={styles.container}>
        <AutoComplete
          dataSource={this.props.products}
          filter={AutoComplete.caseInsensitiveFilter}
          hintText="Pesquisar"
          onNewRequest={this.handleSelect}
          onUpdateInput={this.handleUpdateInput}
          searchText={this.state.searchText}
        />
      </div>
    )
  }
}

module.exports = OrderFilters;
