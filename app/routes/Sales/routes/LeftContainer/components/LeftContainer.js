import React, { Component } from 'react';
import OrderFilters from './OrderFilters';

const styles = {};

styles.container = {
  backgroundColor: '#e5e5e5',
  height: 'inherit',
  width: '100%'
};

const products = [];

for (var i = 1; i < 11; i++) {
   products.push({
     value: i,
     text: 'Product ' + i
   });
}

const dataSource2 = ['12345', '23456', '34567'];

class LeftContainer extends Component {
  render() {
    return(
      <div className="left-container" style={styles.container}>
        <OrderFilters products={products} addItem={this.props.addItem}/>
        <ul>
          {this.props.items.map((item) => {
            return <li key={item.id}>{item.name}</li>
          })}
        </ul>
      </div>
    )
  }
}

module.exports = LeftContainer;
