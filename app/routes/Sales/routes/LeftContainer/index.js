module.exports = {
  path: 'leftContainer',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      cb(null, require('./components/LeftContainer'))
    })
  }
}
