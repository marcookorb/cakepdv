module.exports = {
  path: 'rightContainer',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      cb(null, require('./components/RightContainer'))
    })
  }
}
