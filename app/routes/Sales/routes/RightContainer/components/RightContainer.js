import React, { Component } from 'react';

const styles = {};

styles.container = {
  backgroundColor: '#656565',
  boxShadow: 'rgb(136, 136, 136) -7px -4px 4px -6px',
  height: 'inherit',
  width: '50%'
}

class RightContainer extends Component {
  render() {
    return(
      <div className="right-container" style={styles.container}>
        <h2>Right Container</h2>
        <ul>
          {this.props.items.map((item) => {
            return <li key={item.id}>{item.name}</li>
          })}
        </ul>
      </div>
    )
  }
}

module.exports = RightContainer;
