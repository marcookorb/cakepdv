import React from 'react'
import axios from 'axios'

class Customers extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      customers: []
    }
  }

  componentDidMount() {
    axios({
      baseURL: 'http://192.168.18.17:6543',
      url: '/api/customer/all',
      method: 'get',
      headers: {
        'x-cake-token': '130f35a3ff1df0dd9bdf'
      }
    }).then(res => {
      console.log(JSON.parse(res.data.list));
      this.setState({
        customers: JSON.parse(res.data.list)
      })
    });
  }

  render() {

    return (
      <div>
        <h2>Customers</h2>
        <ul>
          {this.state.customers.map(customer =>
            <li key={customer.id}>{customer.name || '-'}</li>
          )}
        </ul>
      </div>
    )
  }

}

module.exports = Customers
